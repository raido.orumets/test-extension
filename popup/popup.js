let data = {
    tab: null,
    jira: null,
    issue: null,
    user: null,
};

const executeScript = (tabId, func) =>
    new Promise((resolve) => {
        chrome.scripting.executeScript({ target: { tabId }, function: func }, resolve);
    });

(async () => {
    const [tab] = await chrome.tabs.query({ active: true, currentWindow: true });

    data.tab = tab;

    const [{ result: content }] = await executeScript(tab.id, () => {
        return {
            jira: {
                baseUrl: document.querySelector("meta[name='ajs-base-url']").getAttribute("content").trim(),
                token: document.querySelector("meta[name='atlassian-token']").getAttribute("content").trim(),
            },
            issue: {
                key: document.querySelector("#issue-content #key-val").innerText.trim(),
                type: document.querySelector("#issuedetails #type-val").innerText.trim().toLowerCase(),
                title: document.querySelector("#issue-content #summary-val").innerText.trim(),
            },
            user: {
                username: document.querySelector("meta[name='ajs-remote-user']").getAttribute("content").trim(),
                fullname: document.querySelector("meta[name='ajs-remote-user-fullname']").getAttribute("content").trim(),
                locale: document.querySelector("meta[name='ajs-user-locale']").getAttribute("content").trim(),
            },
        };
    });

    data = { ...data, ...content };

    console.log("Chrome extension", data);
    alert(data.issue.key);
})();
