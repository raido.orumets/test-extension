function createElementFromHtml(html, trim = true) {
    html = trim ? html.trim() : html;
    if (!html) return null;
    const template = document.createElement("template");
    template.innerHTML = html;
    const result = template.content.children;
    if (result.length === 1) return result[0];
    return result;
}

const sidebarModule = createElementFromHtml(
    `<div id="votingmodule" class="module toggle-wrap">
        <div id="votingmodule_heading" class="mod-header">
            <h3 class="toggle-header" id="votingmodule-label">
                <button class="aui-button toggle-title" aria-label="People" aria-controls="votingmodule" aria-expanded="true" resolved="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14">
                        <g fill="none" fill-rule="evenodd">
                            <path d="M3.29175 4.793c-.389.392-.389 1.027 0 1.419l2.939 2.965c.218.215.5.322.779.322s.556-.107.769-.322l2.93-2.955c.388-.392.388-1.027 0-1.419-.389-.392-1.018-.392-1.406 0l-2.298 2.317-2.307-2.327c-.194-.195-.449-.293-.703-.293-.255 0-.51.098-.703.293z" fill="#344563"></path>
                        </g>
                    </svg>
                    <span class="aui-toggle-header-button-label">Voting (ICE)</span>
                </button>
            </h3>
            <ul class="ops"></ul>
        </div>
        <div class="mod-content"> 
            <div id="votingiframe" class="item-details"></div>
        </div>
    </div>`
);

const iframe = document.createElement("iframe");
iframe.src = chrome.runtime.getURL("popup/popup.html");
iframe.setAttribute("style", "border: none; width: 100px; height: 100px;");
iframe.setAttribute("scrolling", "no");
iframe.setAttribute("allow", "");

const jiraElement = document.getElementById("peoplemodule");
jiraElement.parentNode.insertBefore(sidebarModule, jiraElement.nextSibling);

const content = document.getElementById("votingiframe");
content.appendChild(iframe);
